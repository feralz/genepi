﻿// This is an example of Leaflet usage; you should modify this for your needs.
var mymap = L.map('mapid', { zoomControl: false }).setView([45.750000, 4.850000], 12);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWdvdWJzIiwiYSI6ImNrcHpqOG45NDAwZGsyb3JtYTQ4cGRqc3YifQ.l3alAAQuPyWvXl-DMBDyew'
}).addTo(mymap);

let Coords = {};

var optionsDepart = {
    position: 'topleft',
    expanded: true,
    placeholder: 'Point de départ',
    title: 'Point de départ',
    overrideBbox: true,
    panToPoint:true
};


var optionsStop = {
    position: 'topleft',
    expanded: true,
    placeholder: 'Point de destination',
    title: 'Point de destination'
};


var geocoderControl = new L.control.geocoder('pk.baa715e306bf4f0c58fd0a0f6621e1f9', optionsDepart).addTo(mymap).on('select', function (e) {
    Coords.startLat = e.latlng.lat;
    Coords.startLong = e.latlng.lng;
});

var geocoderControl2 = new L.control.geocoder('pk.baa715e306bf4f0c58fd0a0f6621e1f9', optionsStop).addTo(mymap).on('select', function (e) {
    Coords.stopLat = e.latlng.lat;
    Coords.stopLong = e.latlng.lng;
    Coords.TransportType = "all";

    broker.emit(Coords);
});


function carroute() {
    //sendAjax("api/carroute");
    Coords.TransportType = "driving-car";
    broker.emit(Coords);

    //getAllDuration(Coords)

    $('#icon-to-change').removeClass();
    $('#icon-to-change').addClass("fas fa-car fa-2x");
}

function cyclingroute() {
    //sendAjax("api/cyclingroute");
    Coords.TransportType = "cycling-regular";
    broker.emit(Coords);

    $('#icon-to-change').removeClass();
    $('#icon-to-change').addClass("fas fa-biking fa-2x");
}

function footroute() {
    //sendAjax("api/footroute");

    Coords.TransportType = "foot-walking";
    broker.emit(Coords);

    $('#icon-to-change').removeClass();
    $('#icon-to-change').addClass("fas fa-hiking fa-2x");
}

//function sendAjax(url) {
//    $.ajax({
//        type: "POST",
//        data: JSON.stringify(Coords),
//        url: url,
//        contentType: "application/json",
//        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
//    }).done(function (data) {
//        const parsedData = jQuery.parseJSON(data);
//        const coords = parsedData.features[0].bbox;
//        const extraData = {
//            metrics: ["duration"],
//            locations: [[coords[0], coords[1]], [coords[2], coords[3]]]
//        };

//        getAllDuration(JSON.stringify(extraData), url);

//        L.geoJSON(parsedData).addTo(mymap);
//        const additionalData = parsedData.features[0].properties.summary

//        return (Math.round(additionalData.duration / 60) + "min");
//    }).fail(function (e) { console.log(e); });
//}

//$(".leaflet-locationiq-close").on("click", function () {
//    window.$(".leaflet-zoom-animated g .leaflet-interactive").remove();
//});

//function getAllDuration(coords) {
//    console.log(coords);
//    $.ajax({
//        url: 'https://api.openrouteservice.org/v2/matrix/driving-car?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
//        type: 'POST',
//        data: coords,
//        contentType: "application/json",
//        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
//    }).done(function (otherData) {
//        $("#carTime").text(Math.round(otherData.durations[1][0] / 60) + " min");
//        if (Coords.TransportType === "driving-car") {
//            $("#duree").text(Math.round(otherData.duration[1][0] / 60) + "min");
//            $("#distance").text(getDistance(otherData.distance[1][0]));
//        }
//    }).fail(function (e) { console.log(e); });

//    $.ajax({
//        url: 'https://api.openrouteservice.org/v2/matrix/cycling-regular?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
//        type: 'POST',
//        data: coords,
//        contentType: "application/json",
//        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
//    }).done(function (otherData) {
//        $("#cyclingTime").text(Math.round(otherData.durations[1][0] / 60) + " min");
//        if (url == "api/cyclingroute")
//            $("#informations").text("Durée : " + Math.round(otherData.durations[1][0] / 60) + " min");
//    }).fail(function (e) { console.log(e); });

//    $.ajax({
//        url: 'https://api.openrouteservice.org/v2/matrix/foot-walking?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
//        type: 'POST',
//        data: coords,
//        contentType: "application/json",
//        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
//    }).done(function (otherData) {
//        $("#footTime").text(Math.round(otherData.durations[1][0] / 60) + " min");
//        if (url == "api/footroute")
//            $("#informations").text("Durée : " + Math.round(otherData.durations[1][0] / 60) + " min");
//    }).fail(function (e) { console.log(e); });
//}

function getDistance(distance) {
    if (distance < 1000) {
        return distance.toFixed(0) + "m";
    } else {
        return (distance/1000).toFixed(1) + "km";
    }
}
