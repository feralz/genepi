﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Genepi.Settings
{
    public class Config : IConfig
    {
        public MongoDB Mongodb { get; set; }
        public RabbitMQ RabbitMQ { get; set; }
    }

    public interface IConfig
    {
        MongoDB Mongodb { get; set; }
        RabbitMQ RabbitMQ { get; set; }
    }
}
