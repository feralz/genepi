﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Settings.MongoDBNS
{
    public class Collections : ICollections
    {
        public string Tests { get; set; }
    }

    public interface ICollections
    {
        string Tests { get; set; }
    }
}
