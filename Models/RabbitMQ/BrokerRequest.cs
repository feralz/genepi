﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Models
{
    public class BrokerRequest
    {
        public string Id { get; set; }
        public string Message { get; set; }
    }
}
