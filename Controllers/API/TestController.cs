﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Genepi.Services;
using Genepi.Models;

namespace Genepi.Controllers.API
{
    [Route("api")]
    [ApiController]
    public class TestController : Controller
    {
        private readonly TestService _testService;

        public TestController(TestService testService)
        {
            _testService = testService;
        }

        [HttpGet("tests")]
        public ActionResult<List<Test>> Get() => _testService.Get();
        //public ActionResult<String> Get() => "TODO Réza";
        /*
        //TODO remplacer string par le bon truc, on recupere depuis rabbitMQ
        public ActionResult<String> GetItineraire()
        {
            Console.WriteLine("tests called...");
            //TODO coder cette partie en fonction de citymapper ou une alternative
            //TODO: le return doit envoyer a rabbitMQ
            return "TODO";
        }
        */

        [HttpGet("{id:length(24)}", Name = "GetTest")]
        public ActionResult<Test> Get(string id)
        {
            var test = _testService.Get(id);

            if (test == null)
            {
                return NotFound();
            }

            return test;
        }

        [HttpPost]
        public ActionResult<Test> Create(Test test)
        {
            _testService.Create(test);

            return CreatedAtRoute("GetTest", new { id = test.Id.ToString() }, test);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Test testIn)
        {
            var test = _testService.Get(id);

            if (test == null)
            {
                return NotFound();
            }

            _testService.Update(id, testIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var test = _testService.Get(id);

            if (test == null)
            {
                return NotFound();
            }

            _testService.Remove(test.Id);

            return NoContent();
        }
    }
}
